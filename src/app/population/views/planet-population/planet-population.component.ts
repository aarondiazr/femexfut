import { Component, OnInit } from '@angular/core';
import { PopulationService } from '../../services/population.service';
import { Planets, Planet } from 'src/app/libs/models/planet.models';
import { forkJoin, merge, of } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Character } from 'src/app/libs/models/people.models';

@Component({
  selector: 'app-planet-population',
  templateUrl: './planet-population.component.html',
  styleUrls: ['./planet-population.component.scss']
})
export class PlanetPopulationComponent implements OnInit {

  constructor(
    private populationService: PopulationService,
    private http: HttpClient
  ) { }

  private nextPage: string;
  private previousPage: string;
  private page = 0;
  private numResults = 10;

  public planetsData: any;
  public loadPlanets = true;

  requestPlanets(url?: string) {
    this.loadPlanets = true;
    this.page = this.nextPage === url ? this.page += 1 : this.previousPage === url ? this.page -= 1 : 0;
    const request = url ? this.populationService.getPlanets(url) : this.populationService.getPlanets();
    request
    .pipe(
      mergeMap( (res: Planets) => {
        if (this.numResults > 10) {
          return this.populationService.recursiveQuery(res.results, res.next, this.numResults);
        }
        return of(res);
      })
    )
    .subscribe((res: Planets) => {
      if (res.count > 0) {
        this.planetsData = res.results;
        this.previousPage = res.previous;
        this.nextPage = res.next;
      }
      this.loadPlanets = false;
    });
  }

  showNumberResults(amount: number) {
    this.numResults = amount;
    this.requestPlanets();
  }


  loadNamesCharacters(data: string []) {
    return forkJoin(data.map(url => this.http.get(url)));
  }

  ngOnInit() {
    this.requestPlanets();
  }

}
