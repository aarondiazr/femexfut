import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetPopulationComponent } from './planet-population.component';

describe('PlanetPopulationComponent', () => {
  let component: PlanetPopulationComponent;
  let fixture: ComponentFixture<PlanetPopulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetPopulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetPopulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
