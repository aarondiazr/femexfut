import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetPopulationComponent } from './views/planet-population/planet-population.component';

const routes: Routes = [
  {
    path: '',
    component: PlanetPopulationComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PopulationRoutingModule { }
