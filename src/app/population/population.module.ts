import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetPopulationComponent } from './views/planet-population/planet-population.component';
import { PopulationService } from './services/population.service';
import { PopulationRoutingModule } from './population-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HideUrlPipe } from './pipes/hideurl.pipe';
import { SpinnerModule } from '../libs/widgets/spinner/spinner.module';

@NgModule({
  declarations: [PlanetPopulationComponent, HideUrlPipe],
  imports: [
    CommonModule,
    HttpClientModule,
    SpinnerModule,
    PopulationRoutingModule
  ],
  providers: [
    PopulationService
  ]
})
export class PopulationModule { }
