import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'hideUrl'})
export class HideUrlPipe implements PipeTransform {
  transform(value: string): any {
    return value.includes('https') ? 'Cargando...' : value;
  }
}
