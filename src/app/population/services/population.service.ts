import { Injectable } from '@angular/core';
import { People, Character } from '../../libs/models/people.models';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, mergeMap } from 'rxjs/operators';
import { Planets, Planet } from 'src/app/libs/models/planet.models';
import { forkJoin, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopulationService {

  constructor(private http: HttpClient) { }

  private url = `${environment.api}planets`;

  getPlanets(url?: string) {
    const request =  url ? this.http.get(url) : this.http.get(this.url);
    return request.
    pipe(
      map((res: Planets) => {
        if (res.count > 0) {
          res.results.map( async (respeople: Planet) => {
            const temporal = await forkJoin(respeople.residents.map(url => this.http.get(url))).toPromise();
            respeople.residents = temporal ? temporal.map( (characterData: Character) => characterData.name) : ['No hay residentes'];
            return respeople;
          });
        }
        return res;
      })
    );
  }

  recursiveQuery(results: Planet[], url: string, numResults: number) {
    return this.getPlanets(url)
    .pipe(
      mergeMap( (res: Planets) => {
        if (res.next && results.length < numResults) {
          return this.recursiveQuery(results.concat(res.results), res.next, numResults);
        }
        res.results = [...results];
        return of(res);
      })
    );
  }
}
