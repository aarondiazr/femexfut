import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'starwars';
  public showSidebar = false;

  sidebarEvent(event: boolean) {
    this.showSidebar = event;
  }
}
