import { Injectable } from '@angular/core';
import { People, Character } from '../../libs/models/people.models';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  private url = `${environment.api}people`;

  constructor(private http: HttpClient) { }

  getPeople(name?: string) {
    return name ? this.http.get(this.url, { params: { search: name} }) : this.http.get(this.url);
  }

  getCharacter(id: number) {
    return this.http.get(`${this.url}/${id}`);
  }
}
