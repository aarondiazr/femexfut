import { CardPreviewComponent } from './../libs/widgets/card-preview/card-preview.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleService } from './services/people.service';
import { AllPeopleComponent } from './views/all-people/all-people.component';
import { InfoCharacterComponent } from './views/info-character/info-character.component';
import { PeopleRoutingModule } from './people-rounting.module';
import { HttpClientModule } from '@angular/common/http';
import { SearchBarComponent } from '../libs/widgets/search-bar/search-bar.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SpinnerModule } from '../libs/widgets/spinner/spinner.module';
import { OrderByPipe } from './pipes/order.pipes';

@NgModule({
  declarations: [
    AllPeopleComponent,
    InfoCharacterComponent,
    SearchBarComponent,
    CardPreviewComponent,
    OrderByPipe
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    SpinnerModule,
    PeopleRoutingModule
  ],
  providers: [
    PeopleService
  ]
})
export class PeopleModule { }
