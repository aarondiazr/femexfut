import { Pipe, PipeTransform } from '@angular/core';
import { Character } from 'src/app/libs/models/people.models';

@Pipe({name: 'orderResult'})
export class OrderByPipe implements PipeTransform {
  transform(array: Character[], filter: string): any {
    if (filter === 'nombre') {
      return array.sort((a: Character, b: Character) => {
        return a.name.toLocaleUpperCase() > b.name.toLocaleUpperCase() ? 1 : -1;
      });
    } else if (filter === 'peso') {
      return array.sort((a: Character, b: Character) => {
        const v1 = a.mass === 'unknown' ? '-1' : a.mass;
        const v2 = b.mass === 'unknown' ? '-1' : b.mass;
        return parseInt(v2, 10) - parseInt(v1, 10);
      });
    } else if (filter === 'altura') {
      return array.sort((a: Character, b: Character) => {
        const v1 = a.height === 'unknown' ? '-1' : a.height;
        const v2 = b.height === 'unknown' ? '-1' : b.height;
        return parseInt(v2, 10) - parseInt(v1, 10);
      });
    } else {
      return array;
    }
  }
}
