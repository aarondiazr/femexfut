import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../../services/people.service';
import { People, Character } from 'src/app/libs/models/people.models';
import { tap, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-all-people',
  templateUrl: './all-people.component.html',
  styleUrls: ['./all-people.component.scss']
})
export class AllPeopleComponent implements OnInit {

  constructor(
    private peopleService: PeopleService,
    private http: HttpClient,
    private route: ActivatedRoute
    ) {
  }

  public results: Character[];
  public nextPage: string;
  public loading = true;
  public orderFilter = '';

  loadAllPeople() {
    this.peopleService.getPeople()
    .pipe(
      tap( res => this.loading = true),
      mergeMap( (res: People) => {
        if (res.next) {
          return this.recursiveQuery(res.results, res.next);
        }
        return of(res);
      })
    )
    .subscribe((res: People) => {
      if (res.count >= 1) {
        this.results = [...res.results];
      }
      this.loading = false;
    });
  }

  recursiveQuery(results: Character[], url: string) {
    return this.http.get(url)
    .pipe(
      mergeMap( (res: People) => {
        if (res.next) {
          return this.recursiveQuery(results.concat(res.results), res.next);
        }
        res.results = [...results];
        return of(res);
      })
    );
  }

  getSearchQuery(consulta: string) {
    if (consulta.length === 0) {
      this.loadAllPeople();
    } else {
      this.peopleService.getPeople(consulta)
      .pipe(
        tap( res => this.loading = true)
      )
      .subscribe((res: People) => {
        if (res.count >= 1) {
          this.results = [...res.results];
        }
        this.loading = false;
      });
    }
  }

  ngOnInit() {
    const orden = this.route.snapshot.queryParams;
    this.loadAllPeople();
    if (orden.ordenar) {
      switch (orden.ordenar) {
        case 'peso': {
          this.orderFilter = 'peso';
          break;
        }
        case 'nombre': {
          this.orderFilter = 'nombre';
          break;
        }
        case 'altura': {
          this.orderFilter = 'altura';
          break;
        }
        default:
          break;
      }
    }
  }
}
