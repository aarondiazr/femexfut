import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../../services/people.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { People, Character } from 'src/app/libs/models/people.models';

@Component({
  selector: 'app-info-character',
  templateUrl: './info-character.component.html',
  styleUrls: ['./info-character.component.scss']
})
export class InfoCharacterComponent implements OnInit {

  constructor(
    private peopleService: PeopleService,
    private http: HttpClient,
    private route: ActivatedRoute
  ) { }

  public loading = true;
  public dataCharacter: Character;

  ngOnInit() {
    const params = this.route.snapshot.paramMap.get('nombre');
    this.peopleService.getPeople(params)
    .subscribe((res: People) => {
      if (res.count > 0) {
        this.dataCharacter = res.results[0];
      }
      this.loading = false;
    });
  }

}
