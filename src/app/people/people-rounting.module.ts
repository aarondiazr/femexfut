import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllPeopleComponent } from './views/all-people/all-people.component';
import { InfoCharacterComponent } from './views/info-character/info-character.component';

const routes: Routes = [
  {
    path: 'personajes',
    component: AllPeopleComponent
  },
  {
    path: 'personaje/:nombre',
    component: InfoCharacterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule { }
