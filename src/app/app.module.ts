import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './libs/widgets/navbar/navbar.component';
import { SidebarComponent } from './libs/widgets/sidebar/sidebar.component';
import { SpinnerComponent } from './libs/widgets/spinner/spinner.component';
import { PeopleModule } from './people/people.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PeopleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
