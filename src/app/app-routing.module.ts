import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path : '' , redirectTo : '/personajes' , pathMatch: 'full' },
  {
    path: '',
    loadChildren: './people/people.module#PeopleModule'
  },
  {
    path: 'residentes',
    loadChildren: './population/population.module#PopulationModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
