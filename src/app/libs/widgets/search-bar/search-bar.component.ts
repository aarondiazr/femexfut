import { FormControl } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchInput = new FormControl('');
  @Output() searchInputEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.searchInput.valueChanges
    .subscribe( res => {
      this.searchInputEvent.emit(res);
    })
  }

}
