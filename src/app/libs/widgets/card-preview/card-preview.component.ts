import { Component, OnInit, Input } from '@angular/core';
import { Character } from '../../models/people.models';

@Component({
  selector: 'app-card-preview',
  templateUrl: './card-preview.component.html',
  styleUrls: ['./card-preview.component.scss']
})
export class CardPreviewComponent implements OnInit {

  private _typeCard = 1; // El 2 muestra informacion completa

  @Input() characterData: Character;
  @Input()
  get typeCard() {
    return this._typeCard;
  }
  set typeCard(value: number) {
    this._typeCard = value;
  }

  constructor() { }

  ngOnInit() {
  }

}
