import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public showMenu = false;

  @Output() openMenuEvent = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  openMenu() {
    this.showMenu = !this.showMenu;
    this.openMenuEvent.emit(this.showMenu);
  }

}
